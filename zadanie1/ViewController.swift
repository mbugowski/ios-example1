//
//  ViewController.swift
//  zadanie1
//
//  Created by Mateusz Bugowski on 16/03/2019.
//  Copyright © 2019 Mateusz Bugowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    //MARK: Properties
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var mealNameLabel: UILabel!
    @IBOutlet weak var przyslowieText: UITextView!
    @IBOutlet weak var przyslowieLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.isEmpty)! {
            mealNameLabel.text = "Imię"
        } else {
            mealNameLabel.text = textField.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        nameTextField.resignFirstResponder()
        return true
    }

    @IBAction func wylosujPrzyslowie(_ sender: UIButton) {
        let wylosowanePrzyslowie = Przyslowie().wylosowanePrzyslowie()
        przyslowieText.text = wylosowanePrzyslowie
    }
}
