//
//  Przyslowie.swift
//  zadanie1
//
//  Created by Mateusz Bugowski on 16/03/2019.
//  Copyright © 2019 Mateusz Bugowski. All rights reserved.
//

import UIKit

class Przyslowie: NSObject {
    let przyslowia: [String] = [
        "Kto pod kim dołki kopie, ten sam w nie wpada",
        "Gdyby kózka nie skakała, to by nóżki nie złamała",
        "Darowanemu koniowi w zęby się nie zagląda",
        "Nie chwal dnia przed zachodem słońca.",
        "Lepszy wróbel w garści niż gołąb na dachu.",
        "Co ma wisieć, nie utonie",
        "Śpiesz się powoli"
    ]
    var przyslowie: String
    
    override init() {
        self.przyslowie = przyslowia[Int.random(in: 0..<przyslowia.count)]
    }
    
    func wylosowanePrzyslowie() -> String {
        return przyslowie
    }
}
